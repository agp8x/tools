#!/usr/bin/env python3
# -*- coding: utf-8 -*- 
from urllib import urlretrieve
from time import strftime,sleep

URL = "http://www.dwd.de/wundk/radar/Webradar_Deutschland.jpg"

while True:
	name = strftime("%Y.%m.%d-%H.%M.jpg")
	try:
		urlretrieve(URL, name)
	except Exception :
		pass
	sleep(60*15)
