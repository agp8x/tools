#!/usr/bin/python3
# -*- coding: utf-8 -*-

import requests, json, datetime

url = "https://www.farming-simulator.com/modHubBEMain.php"
payload={'greenstoneX':1, 'redstoneX':'<user>', 'bluestoneX':'<pass>', 'foobarX':'&nbsp;&nbsp;Login&nbsp;&nbsp;'}
outfile="lsStats.list"

s = requests.Session()
r=s.get('https://www.farming-simulator.com/modHubBEMain.php')
i = r.text.find("action=\"/modHubBEMain.php\"?r=\"")
rand = r.text[(i+30):(i+39)]
r2 = s.post(url + "?r="+rand, data=payload)
if not r2.status_code == 200 or 'style="color:red; font-weight:bold;"' in r2.text:
	print("ERROR")
	with open("stats.list", "a") as output:
		output.write(json.dumps({"error":str(datetime.datetime.now())}))
else:
	data_table = r2.text[r2.text.find("<table>"):r2.text.find("</table>")]
	stats={}
	while data_table.find("modId") > 0:
		data_table = data_table[data_table.find("modId"):]
		item = data_table[data_table.find("modId"):data_table.find("deleteMod")]
		stat = []
		for line in item.splitlines():
			line = line.strip()
			line = line.replace('<td><span style="">',"")
			line = line.replace("</span></td>","")
			if line.startswith("<") or line.endswith(">"):
				continue
			stat.append(line)
		stat.append(str(datetime.datetime.now()))
		mod = item.split('"')[0].split("=")[1]
		stats[mod] = stat
		data_table = data_table[data_table.find("deleteMod"):]
	with open(outfile, "a") as output:
		output.write(json.dumps(stats))
		output.write("\n")
