#!/usr/bin/python3
# -*- coding: utf-8 -*-

""" move files into subfolders"""

import argparse
import logging
import os

logging.basicConfig(format='%(levelname)s : %(message)s', level=logging.INFO)

def get_arguments():
    """arguments"""
    return [
        ["-e", "--extension", "file extension", ".mp3"],
        ["-s", "--seperator", "seperator", "-"],
        ["-a", "--empty", "placeholder for empty titles", "advertisement"]]

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="distribute")
    for arg in get_arguments():
        parser.add_argument(arg[0], arg[1], help=arg[2], default=arg[3])
    args = parser.parse_args()
    logging.info("starting distribution")
    for afile in os.listdir('.'):
        s = args.seperator
        if afile.endswith(args.extension):
            logging.info(afile)
            parts = afile.split(args.seperator)
            first_part = parts[0].strip()
            if len(first_part) == 0:
                first_part = args.empty
            new_file = os.path.join(first_part, afile)
            i = 1
            while os.path.exists(new_file):
                logging.warning("file exists: %s", new_file)
                base = parts[-1].strip(args.extension)
                base = base +" (%i)" % i
                incremented = s.join(parts[:-1]) + s + base + args.extension
                new_file = os.path.join(first_part, incremented)
                logging.info("new name: %s", new_file)
                i = i + 1
            logging.info(new_file)
            os.renames(afile, new_file)
