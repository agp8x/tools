#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('GdkPixbuf', '2.0')
from gi.repository import Gtk,GdkPixbuf
from io import BytesIO
import argparse, qrcode

class Window(object):
	def __init__(self, content):
		self.window = Gtk.Window()
		self.window.set_title("QR")
		self.window.set_default_size(150, 150)
		self.window.connect("destroy", self.destroy)
		self.window.connect("key-press-event", self.on_key)
		image = Gtk.Image()
		qr = self.__qr__(content)
		image.set_from_pixbuf(qr)
		self.window.add(image)
		image.show()
		self.window.show()
	def destroy(self, data=None):
		Gtk.main_quit()
	def main(self):
		Gtk.main()
	def on_key(self, event, data=None):
		self.destroy()
	def __qr__(self, content):
		qr = qrcode.QRCode(box_size=5)
		qr.add_data(content)
		qr.make(fit=True)
		im = qr.make_image()
		return self.image2pixbuf(im)
	def image2pixbuf(self, im):
		buff = BytesIO()
		im.save(buff, 'ppm')
		contents = buff.getvalue()
		buff.close()
		loader = GdkPixbuf.PixbufLoader.new_with_type('pnm')
		loader.write(contents)
		pixbuf = loader.get_pixbuf()
		loader.close()
		return pixbuf

if __name__=="__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("content")
	args = parser.parse_args()
	window = Window(args.content)
	window.main()
