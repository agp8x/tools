#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Author: agp8x

import re
from urllib.request import urlopen
import argparse

RESOLUTIONS = ['hd', 'sd']
FORMATS = ['webm', 'webm2', '23c3', '22c3', '25c3', 'mkv',  'mp45', 'mp44', 'mp43', 'mp42', 'mp4']
CDN_BASE = "https://cdn.media.ccc.de/congress/"

parser = argparse.ArgumentParser(description='Generate urls for ccc-congresses')
parser.add_argument("-c", "--congress", default="31c3", required=True, help="abbreviation of the congress, e.g. '%(default)s'")
parser.add_argument("-y", "--year", type=int, default=2014, required=True, help="year of the congress, e.g. %(default)s")
parser.add_argument("-s", "--sd", default=False, action='store_true', help="generate SD-URLs, default: Off")
parser.add_argument("-m", "--mp4", default=False, action='store_true', help="prefer MP4 over WebM, default: Off")
args=parser.parse_args()

event = args.congress.upper()
year = int(args.year)
resolution = RESOLUTIONS[ 1 if args.sd else 0]
if args.mp4:
	order = sorted(FORMATS)
else:
	order = FORMATS
INPUT = {
	'webm' : CDN_BASE + event.upper() +"/webm-" + resolution + "/",
	'mp4' : CDN_BASE + event.upper() +"/h264-" + resolution + "/",
	'mp42' : CDN_BASE + event.upper() +"/h264/",
	'mp43' : CDN_BASE + event.upper() +"/mp4-hd/",
	'mp44' : CDN_BASE + event.upper() +"/mp4/",
	'mp45' : CDN_BASE + event.upper() +"/mp4-h264-HQ/",
	'23c3' : CDN_BASE + event.upper() +"/video/",
	'22c3' : CDN_BASE + event.upper() +"/lectures/video/mp4-avc/320x240/",
	'25c3' : CDN_BASE + event.upper() +"/video_h264_720x576/",
	'mkv' : CDN_BASE + event.upper() +"/matroska/",
	'webm2' : CDN_BASE + event.upper() +"/webm/",
}
FAHRPLAN = "https://events.ccc.de/congress/" + str(year) + "/Fahrplan/"
FAHRPLAN_XML = FAHRPLAN + "schedule.xml"
FAHRPLAN_EVENT = "events/"

if year < 2004:
	raise "These old days are not supported, please visit http://cdn.media.ccc.de/congress/"

print("Generate links for " + event + " (" + str(year) + ") in "+ resolution + ", prefering " + order[0] + " ...")

ids = []
urls = []

def id2fahrplan(id):
	return FAHRPLAN + FAHRPLAN_EVENT + str(id) + ".html"

for format in order:
	base = INPUT[format]
	try:
		#print("\t load: " + base)
		input = urlopen(base).readlines()
	except :
		print("ERROR LOADING URL '"+base+"'")
		input = []
	for line in input:
		line = str(line)
		url = ""
		for ev in (event, event.lower()):
			match = re.search('a\Whref="(.+)">' + ev, line)
			if not match is None:
				url = base + match.group(1)
			else:
				match = re.search('<a\Whref="(.+mp4)">', line)
				if not match is None:
					url = base + match.group(1)
			m = re.search('a\Whref="' + ev + '-(\d+)-.+">' + ev, line)
			if not url is "":
				if not m is None:
					id = int(m.group(1))
				else:
					id = url
				if len(ids) == 0:
					ids.append(id)
					urls.append(url)
				if not id in ids and type(ids[0]) is type(id):
					ids.append(id)
					urls.append(url)
try:
	fahrplan = urlopen(FAHRPLAN_XML).readlines()
except:
	try:
		new_url = FAHRPLAN_XML.replace('.xml', '.en.xml')
		fahrplan = urlopen(new_url).readlines()
	except:
		print("VALIDATION IMPOSSIBLE")
		fahrplan = []
missing = []
for line in fahrplan:
	match = re.search("<event.+id='(\d+)'>", str(line))
	if not match is None:
		id = match.group(1)
		if not (id in ids or int(id) in ids):
			missing.append(id2fahrplan(id))
print( str(len(urls)) + " Urls were found, while " + str(len(missing)) + " are missing")

if not len(urls) == 0:
	with open(event + '-' + resolution + '-urls', 'w') as found:
		found.write("\n".join(urls))
if not len(missing) == 0:
	with open(event + '-' + resolution + '-missing', 'w') as missed:
		missed.write("\n".join(missing))

