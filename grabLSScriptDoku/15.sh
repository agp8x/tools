#!/bin/bash
host="www.ls-mods.de/"
starturl=$host"scriptDocumentation.php?language=de"
extract_lua(){
	file="$1"
	out=${file%.*}
	cat $file|grep "code-listing-code"|cut -d">" -f5-|sed s/'<[^>]\+>'//g|sed s/'&quot;'/'"'/g|sed s/'&gt;'/'>'/g|sed s/'&lt;'/'<'/g|sed s/'&amp;'/'&'/g > $out
	grep -E '&.*;' --color -nH $out
}
iterate_classes(){
	classes="$1"
	for class in $classes; do
		file=${class#*,}
		url=$host${class%,*}
		echo $file
		wget -q -O $file.html "$url"
	done
}
iterate_scripts(){
	links="$1"
	for link in $links; do
		file=${link#*,}
		url=$host${link%,*}
		path=${url#*=}
		path=${path%/*}
		path=${path%&*}
		mkdir -p $path
		file=$path/$file.html
		wget -q -O "$file" "$url"
		extract_lua $file
		rm $file
	done
}

classes=$(curl -s $starturl|grep "?class="|cut -d"=" -f3-10|cut -d"<" -f1|sed s/">"/","/|sed s/'"'//g)
iterate_classes "$classes"

scripts=$(curl -s $starturl|grep "lua_file"|cut -d"=" -f3-10|cut -d"<" -f1|sed s/">"/","/|sed s/'"'//g)
iterate_scripts "$scripts"
