This is a crawler for br.de, which extracts links for all videos it can find in the a input-file, which shall list URLs to crawl.

Example for URL-list:
		http://www.br.de/fastnacht-in-franken/sendungsarchiv/index.html
		http://www.br.de/fastnacht-in-franken/fastnacht-2000-dueringer100.html
		http://www.br.de/fastnacht-in-franken/fastnacht-in-franken-2000-video-kuhn100.html
