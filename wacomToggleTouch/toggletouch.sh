#!/bin/bash
id="`xsetwacom --list devices|grep "touch" | cut -d":" -f2|cut -d"	" -f1`"
set=`xsetwacom get $id touch`;
if [ "$set" == "on" ]
	then
		xsetwacom set $id touch off;
	else
		xsetwacom set $id touch on;
fi;
