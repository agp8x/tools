#!/bin/bash
PAUSE="600"
BASE_DIR="."
DIR=`date +"%x"`

mkdir -p $DIR

while true
do
	TIME=`date +"%x+%H+%M"`
	import -window root $BASE_DIR/$DIR/$TIME.png
	sleep $PAUSE
done

exit
