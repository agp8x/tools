#!/usr/bin/env python3
# -*- coding: utf-8 -*-  
import unittest
import os 
from bundler import Bundler
from zipfile import ZipFile

class TestBundler(unittest.TestCase):
	def setUp(self):
		self.maxDiff=None
		if os.path.exists('asdf.zip'):
			os.remove('asdf.zip')
		if os.path.exists('asdf_wild.zip'):
			os.remove('asdf_wild.zip')
		self.bundler=Bundler()
	def tearDown(self):
		if os.path.exists('asdf.zip'):
			os.remove('asdf.zip')
		if os.path.exists('asdf_wild.zip'):
			os.remove('asdf_wild.zip')
	
	def test_constructor(self):
		self.assertIsNotNone(Bundler())
		self.assertTrue(self.bundler.overwrite)
		self.bundler.toggleOverwrite()
		self.assertFalse(self.bundler.overwrite)
		bundler2=Bundler(False)
		self.assertFalse(bundler2.overwrite)
	def test_readFile(self):
		self.assertRaises(IOError, self.bundler.readFile, '')
		self.assertTrue(self.bundler.readFile())
		self.assertTrue(self.bundler.readFile('bundle'))
		self.assertIsNotNone(self.bundler.bundle)
		bundle=open('bundle')
		content=bundle.readlines()
		bundle.close()
		self.assertEqual(self.bundler.bundle, content)
	def test_parseBundle(self):
		self.bundler.readFile('bundle')
		self.bundler.parseBundle()
		self.assertEqual("asdf",self.bundler.name)
		self.assertEqual(['bundle', 'bundler.py', 'bundler.test.py', 'subdir/bundle2'],self.bundler.ziplist)
	def test_parseBundle2(self):
		self.bundler.readFile()
		self.bundler.parseBundle()
		self.assertEqual("asdf",self.bundler.name)
		self.assertEqual(['bundle', 'bundler.py', 'bundler.test.py', 'subdir/bundle2'],self.bundler.ziplist)
	def test_parseWildBundle(self):
		self.bundler.readFile('wildBundle')
		self.bundler.parseBundle()
		self.assertEqual("asdf_wild",self.bundler.name)
		self.assertEqual(['bundle', 'bundler.py', 'bundler.test.py', 'subdir/bundle2', 'subdir/bundle2 (Kopie)', 'subdir/bundle2 (weitere Kopie)'],self.bundler.ziplist)
	def test_zip(self):
		self.bundler.readFile('bundle')
		self.bundler.parseBundle()
		self.bundler.zip()
		self.bundler.zip()
		self.assertTrue(os.path.exists('asdf.zip'))
		zip=ZipFile('asdf.zip','r')
		self.assertEqual(zip.namelist(), ['asdf/bundle', 'asdf/bundler.py', 'asdf/bundler.test.py', 'asdf/subdir/bundle2'])
		zip.close()
		self.bundler.toggleOverwrite()
		self.assertRaises(IOError, self.bundler.zip)
		os.remove('asdf.zip')
	def test_wildcardZip(self):
		self.bundler.readFile('wildBundle')
		self.bundler.parseBundle()
		self.bundler.zip()
		self.assertTrue(os.path.exists('asdf_wild.zip'))
		zip=ZipFile('asdf_wild.zip','r')
		self.assertEqual(zip.namelist(), ['asdf_wild/bundle', 'asdf_wild/bundler.py', 'asdf_wild/bundler.test.py', 'asdf_wild/subdir/bundle2', 'asdf_wild/subdir/bundle2 (Kopie)', 'asdf_wild/subdir/bundle2 (weitere Kopie)'])
		zip.close()
		self.bundler.toggleOverwrite()
		self.assertRaises(IOError, self.bundler.zip)
		os.remove('asdf_wild.zip')
	def test_zzzchangedir(self):
		os.chdir('..')
		self.bundler.dir="bundler"
		self.bundler.readFile('bundler/rem_bundle')
		self.bundler.parseBundle()
		self.bundler.zip()
		self.bundler.zip()
		self.assertTrue(os.path.exists('asdf-rem.zip'))
		zip=ZipFile('asdf-rem.zip','r')
		self.assertEqual(zip.namelist(), ['asdf-rem/bundler/bundle', 'asdf-rem/bundler/bundler.py', 'asdf-rem/bundler/bundler.test.py', 'asdf-rem/bundler/subdir/bundle2'])
		zip.close()
		self.bundler.toggleOverwrite()
		self.assertRaises(IOError, self.bundler.zip)
		os.remove('asdf-rem.zip')
		os.chdir('bundler')
		
	
if __name__ == '__main__':
	unittest.main()
