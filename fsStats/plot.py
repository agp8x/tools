#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json, subprocess
from datetime import datetime

with open("config.json") as fp:
	config = json.load(fp)

GNUPLOT_SCRIPT="""#!/usr/bin/gnuplot
reset
set terminal png size 720, 1080
set multiplot layout 2,1
set xdata time
set timefmt "%Y-%m-%d %H:%M:%S"
set format x "%d.%m \\n%H:%M"
set grid xtics;
set grid ytics;
set key below

set size 1,0.75
set origin 0,0.25
set title "accesses"

file='{}'
ids = "{}"
titles = "{}"

dls(id)="<awk -F , '$1 == ".id." {{print $5,$NF}}' ".file
rat(id)="<awk -F , '$1 == ".id." {{print $6,$NF}}' ".file

plot for [i = 1:words(ids)] dls(word(ids, i)) using 2:1 title word(titles, i) with linespoints
set size 1,0.25
set title "ratings"
set key off
set timestamp
plot for [id in ids] rat(id) using 2:1 title id with linespoints"""

mods = {}
with open(config['listFile']) as inputFile:
	for line in inputFile:
		data = line.strip().split(",")
		if not data[0] in mods:
			mods[data[0]] = "{}({})".format(data[1].replace(" ","_"), data[0])
with open(config['plotScript'],"w") as plot:
	ids = " ".join(sorted(mods))
	titles = " ".join([mods[i] for i in sorted(mods)])
	plot.write(GNUPLOT_SCRIPT.format(config['listFile'], ids, titles))
with open(config['plotFile'], "wb") as plot:
	subprocess.call(["gnuplot", config['plotScript']], stdout=plot)
