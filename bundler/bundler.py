#!/usr/bin/env python3
# -*- coding: utf-8 -*- 

"""
Create a ZIP-archive based on a list(->Bundlefile) of files/wildcards

Format of Bundlefile:
	<name of the zip>
	<relative path>
	<absolute path>
	<path>/*
	<path>/
	<folder>
	<file>
	<etc>

If not Bundler#readFile(<filename>) is used, the value in bundle#bundle_file (default: "bundle") is used
"""

import os 
import sys
from fnmatch import fnmatch
from zipfile import ZipFile

bundle_file="wildBundle"

class Bundler(object):
	"""docstring for Bundler"""
	def __init__(self, overwrite=True):
		super(Bundler, self).__init__()
		self.bundle=None
		self.ziplist=[]
		self.name=""
		self.overwrite=overwrite
		self.dir="."
		self.targetdir="."

	def readFile(self,file=bundle_file):
		""" read bundlefile and store its content"""
		if not os.path.exists(file):
			raise IOError("[ERROR]: bundlefile '"+file+"' not found")
		else:
			bundle=open(file)
			content=bundle.readlines()
			bundle.close()
			self.bundle=content
			return True
	
	def parseBundle(self):
		""" parse contents of self.bundle """
		if self.bundle is None:
			try:
				self.readFile()
			except IOError as e:
				raise
		for i, x in enumerate(self.bundle):
			if(i == 0):
				self.name=x.strip()
			else:
				match=self.parseFilename(x)
				self.ziplist += match
	
	def parseFilename(self, name):
		name=name.strip()
		if "*" in name or "?" in name:
			splited=name.split("/")
			list=[]
			parent=self.dir+'/'+"/".join(splited[:-1])
			files = os.listdir(parent)
			try:
				for file in files:
					if os.path.isdir(parent+"/"+file):
						self.bundle += [parent+"/"+file+"/*"]
					elif fnmatch(file, splited[-1:][0]) and not file == self.targetdir+"/"+self.name+".zip" :
						path="/".join(splited[:-1]+[''])+file
						list+=[path]
				return list
			except FileNotFoundError:
				print("'"+name+"' not found, skipping", file=sys.stderr)
				return []
		else:
			if os.path.isdir(name):
				self.bundle += [name + "/*"]
				return []
			else:
				return [name]
	
	def zip(self):
		""" create archive """
		if self.ziplist == []:
			self.parseBundle()
		zipname=self.targetdir+"/"+self.name+".zip"
		if self.name == "":
			zipname = self.targetdir+"/bundle.zip"
		if os.path.exists(zipname):
			if self.overwrite:
				os.remove(zipname)
			else:
				raise IOError("[ERROR]: File '"+zipname+"' already exists")
		zip=ZipFile(zipname,'w')
		for x in self.ziplist:
			zip.write(x, self.name+"/"+x)
		zip.close()
	
	def toggleOverwrite(self):
		self.overwrite=not self.overwrite

if __name__ == '__main__':
	""" sample usage """
	bundler = Bundler()
	if len(sys.argv) >1 :
		if "-o" in sys.argv:
			bundler.toggleOverwrite()
			sys.argv.remove("-o")
		else:
			bundle_file=sys.argv[1]
			bundler.readFile(bundle_file)
			bundler.parseBundle()
			bundler.name=""
	try:
		#bundler.readFile(bundle_file)
		#bundler.parseBundle()
		bundler.zip()
	except IOError as e:
		print(e, file=sys.stderr)
