#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json
import subprocess
from datetime import datetime
files = {
	'input': 'sampleData',
	'plotable': 'plotable',
	'plotScript': 'plot.gp',
	'plot': 'plot.png'
}

stats = []
biggest = {}
with open(files['input']) as inputFile:
	for line in inputFile:
		stat = {}
		obj = json.loads(line)
		for mod in obj:
			if not 'time' in stat:
				time = datetime.strptime(obj[mod][-1],"%Y-%m-%d %H:%M:%S.%f")
				stat['time'] = datetime.strftime(time, "%d.%m.%y%H:%M:%S")
			stat[mod]= (obj[mod][2], obj[mod][3])
		stats.append(stat)
		if len(stat) > len(biggest):
			biggest = stat
if len(stats) > 0:
	with open(files['plotable'], "w") as out:
		order = ['time']
		for key in sorted(biggest):
			if not key in order:
				order.append(key)
		out.write(",".join(order)+"\n")
		for stat in stats:
			for key in order:
				if not key in stat:
					stat[key] = ('0','0')
				string = stat[key] if len(stat[key]) >2 else ",".join(stat[key])
				out.write(string+",")
			out.write("\n")
	with open(files['plotScript'],"w") as plot:
		plot.write("""#!/usr/bin/gnuplot
reset
set terminal png size 720, 1080
set multiplot layout 2,1
set datafile separator ","
set xdata time
set timefmt "%d.%m.%y%H:%M:%S"
set format x "%d.%m \\n%H:%M"
set grid xtics;
set grid ytics;
set key autotitle columnhead
set key below

set size 1,0.75
set origin 0,0.25
set title "accesses"
plot""")
		string = ""
		for i in range(1, len(order)):
			string+=" '"+files['plotable']+"' using 1:"+str(i*2)+" title'"+order[i]+"' with linespoints,"
		string = string.strip(",")
		plot.write(string)
		plot.write("""
set size 1,0.25
set title "ratings"
set key off
plot """)
		string = ""
		for i in range(1, len(order)):
			string+=" '"+files['plotable']+"' using 1:"+str((i*2)+1)+" title'"+order[i]+"' with linespoints,"
		string = string.strip(",")
		plot.write(string)
	with open(files['plot'], "wb") as plot:
		subprocess.run(["gnuplot", files['plotScript']], stdout=plot)
