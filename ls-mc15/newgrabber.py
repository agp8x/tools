#!/usr/bin/python3
# -*- coding: utf-8 -*-

URL= "http://www.farming-simulator.com/mods.php?lang=de&title=fs2015&filter=contest&page=0"
URLS=[
	"http://www.farming-simulator.com/mods.php?lang=de&title=fs2015&filter=contest&page=0",
	"http://www.farming-simulator.com/mods.php?lang=de&title=fs2015&filter=contest&page=1",
	"http://www.farming-simulator.com/mods.php?lang=de&title=fs2015&filter=contest&page=2",
	"http://www.farming-simulator.com/mods.php?lang=de&title=fs2015&filter=contest&page=3"
]
BASE="http://www.farming-simulator.com/"

import urllib.request
import xml.etree.ElementTree as ET
import re
asdf=open("_links", "w")
for url in URLS:
	f = urllib.request.urlopen(url)


	for line in f.read().split(b"\n"):
		line = line.decode("utf-8")
		if not line.find("mod.php?") == -1:
			#print(line)
			parts = line.split('"')
			if len(parts) == 5:
				continue
			url = parts[3]
			#print(url)
			print("LOAD: "+BASE+url) 
			g = urllib.request.urlopen(BASE+url)
			desc = g.read().decode("utf-8")
			#print(desc)
			for line in desc.split("\n"):
				if ".zip" in line:
					print(line)
					url=re.search("(?P<url>http://[^\s]+.zip)",line).group('url')
					print(url)
					asdf.write(url+"\n")

		asdf.flush()
