#!/usr/bin/python3
# -*- coding: utf-8 -*-

import logging
from logging.handlers import TimedRotatingFileHandler
from datetime import time
from flask import Flask,request

app = Flask(__name__)

@app.route("/", defaults={'path':''}, methods=['GET','POST'])
@app.route("/<path:path>", methods=['GET','POST'])
def index(path):
	app.logger.info(request.data.decode("utf8"))
	return "[\"ok\"]"

if __name__ == "__main__":
	handler = TimedRotatingFileHandler("log", when="D", interval=1, atTime=time.min)
	handler.setLevel(logging.INFO)
	handler.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
	app.logger.addHandler(handler)
	app.logger.setLevel(logging.INFO)
	app.run(host="0.0.0.0", port=5000)
