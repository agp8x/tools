#!/bin/bash
mkdir $1
pdfimages $1.pdf $1/bild
mogrify -format png $1/*.ppm
rm $1/*.ppm
