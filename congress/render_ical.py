#!/usr/bin/python3
# -*- coding: utf-8 -*-

ROOMS=['Hall 1', 'Hall 2', 'Hall 6', 'Hall G']
URL="https://events.ccc.de/congress/XXXX/Fahrplan/schedule.ics"
TIMEFORMAT="%d. %H:%M"

from urllib.request import urlopen
from icalendar import Calendar, Event
import argparse

parser = argparse.ArgumentParser(description='render simplified html-table of congress from iCalendar url/file')
parser.add_argument("-u", "--url", default=URL, required=False, help="url to ical, default: '%(default)s'")
parser.add_argument("-y", "--year", default=False, required=True, help="year to replace 'XXXX' in --url with")
parser.add_argument("-f", "--file", default=False, required=False, help="local file (skips loading URL), e.g. 'schedule.ics', default: False")
parser.add_argument("-t", "--time", default=TIMEFORMAT, required=False, help="format for time, default: '%(default)s'")
parser.add_argument("-r", "--rooms", nargs='+',default=ROOMS, required=False, help="List of locations, default: '%(default)s'")
args=parser.parse_args()

url=args.url
if args.year:
    url=url.replace('XXXX', args.year)

if args.file:
    f=open(args.file)
else:
    f=urlopen(url)

c=Calendar.from_ical(f.read())

events={}
for comp in c.walk():
    if not type(comp) is Event:
        continue
    start=comp.get('dtstart').dt.strftime(args.time) +" -> "+comp.get('dtend').dt.strftime(args.time)
    summary=str(comp.get('summary'))
    location=str(comp.get('location'))
    if not start in events:
        events[start]={}
    events[start][location]=summary

html="""<!doctype html>
<table>
<tr><th>time</th>"""
for r in args.rooms:
    html+="<th>"+str(r)+"</th>"
html+="</tr>\n"
for e in sorted(events):
    ev=events[e]
    row="<tr><td>"+str(e)+"</td>"
    for r in args.rooms:
        if r in ev:
            row+="<td>"+str(ev[r])+"</td>"
        else:
            row+="<td></td>"
    row+="</tr>\n"
    html+=row
html+="</table>"

print(html)
