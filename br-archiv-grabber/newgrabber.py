#!/usr/bin/python3
# -*- coding: utf-8 -*-

URL= "http://www.br.de/fastnacht-in-franken/sendungsarchiv/index.html"
BASE="http://www.br.de"

import urllib.request
import xml.etree.ElementTree as ET

f = urllib.request.urlopen(URL)

asdf=open("links", "w")

for line in f.read().split(b"\n"):
	line = line.decode("utf-8")
	if not line.find("link_video") == -1:
		#print(line)
		parts = line.split('"')
		#print(parts)
		url = parts[1]
		g = urllib.request.urlopen(BASE+url)
		desc = g.read().decode("utf-8")
		#print(desc)
		for line in desc.split("\n"):
			if "dataURL" in line:
				#print(line)
				props=line.split("'")
				xml=props[3]
				xmlfile = urllib.request.urlopen(BASE+xml).read().decode("utf-8")
				#print(xmlfile)
				tree = ET.fromstring(xmlfile)
				asset= tree.findall("./video/assets/asset[@type='PREMIUM']/downloadUrl")[0]
				download_url= asset.text
				desc = tree.findall("./video/shareTitle")[0]
				title = tree.findall("./video/title")[0]
				asdf.write(str(download_url)+";"+str(desc.text.replace("\n",""))+";"+str(title.text.replace("\n",""))+"\n")

		#break
	asdf.flush()
	#print(line)
