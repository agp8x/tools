<?php
$files=array(
'linecount.php		',
'pylinecount.py'
);
$linecount=0;
$charcount=0;
echo "<pre>\nDateiname			:Zeilen - Zeichen	- Zeichen/Zeile\n";
foreach($files as $file){
	$chrcnt=0;
	$tmp=array();
	$tmp=file(trim($file));
	$linecount+=sizeof($tmp);
	foreach($tmp as $line){
		$chrcnt+=strlen($line);
	}
	$charcount+=$chrcnt;
	echo $file."	: ".sizeof($tmp)."	- ".$chrcnt."		- ".($chrcnt/sizeof($tmp))."\n";
}
$filenum=sizeof($files);
echo "\n". $filenum ." Dateien:\n- ".$linecount." Zeilen\n- ".$charcount." Zeichen \n- ".($charcount/$linecount)
." Zeichen/Zeile\n- ".($linecount/$filenum)." Zeilen/Datei\n- ".($charcount/$filenum)." Zeichen/Datei\n</pre>\n";
?>
