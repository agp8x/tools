#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import socket
import threading

class PortThread(threading.Thread):
	def __init__(self,start,end):
		threading.Thread.__init__(self)
		self.start1=start
		self.stop=end
	def run(self):
		output=open('ports'+str(self.start1)+'-'+str(self.stop),'w')
		for x in range(self.start1,self.stop):
			sock=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			sock.settimeout(1)
			try:
				sock.connect(("portquiz.net",x))
				print("win "+str(x))
				output.write("win "+str(x)+"\n")
			except Exception:
				#print("fail "+str(x))
				output.write("fail "+str(x)+"\n")
			sock.close()
			output.flush()
		output.close()
##
threads=200
start=0
limit=65535
step=int(limit/threads)
stop=start+step
for x in range(threads):
	asdf=PortThread(start,stop)
	asdf.start()
	start=start+step
	stop=stop+step

