#!/usr/bin/python3
# -*- coding: utf-8 -*-

import requests, json, datetime

with open("config.json") as fp:
	config = json.load(fp)

s = requests.Session()
r=s.get(config['url']+"?lang=de&country=de")
i = r.text.find("action=\"/modHubBEMain.php\"?r=\"")
rand = r.text[(i+30):(i+39)]
r2 = s.post(config['url'] + "?r="+rand, data=config['payload'])
if not r2.status_code == 200 or 'style="color:red; font-weight:bold;"' in r2.text:
	print("ERROR")
	with open(config['listFile'], "a") as output:
		output.write(json.dumps({"error":str(datetime.datetime.now())}))
else:
	data_table = r2.text[r2.text.find("<table"):r2.text.find("</table>")]
	stats=[]
	while data_table.find("modId") > 0:
		data_table = data_table[data_table.find("modId"):]
		item = data_table[data_table.find("modId"):data_table.find("deleteMod")]
		mod = item.split('"')[0].split("=")[1]
		stat = [mod]
		for line in item.splitlines():
			line = line.strip()
			if not line.startswith("<td>"):
				stat.append(line.split('"')[5][1:].split("<")[0])
				continue
			line = line.replace('<td><span style="color:#707070;">',"")
			line = line.replace("</span></td>","")
			if line.startswith("<") or line.endswith(">"):
				continue
			stat.append(line)
		stat.append(str(datetime.datetime.now()))
		stats.append(stat)
		data_table = data_table[data_table.find("deleteMod"):]
	with open(config['listFile'], "a") as output:
		output.write("\n".join(map(lambda x: ",".join(x),stats)))
		output.write("\n")
