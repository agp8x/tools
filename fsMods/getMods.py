#!/usr/bin/python3
# -*- coding: utf-8 -*-

import requests, json, datetime, re, shutil, os, logging, argparse

USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) " \
			 "Ubuntu Chromium/44.0.2403.89 Chrome/44.0.2403.89 Safari/537.36"
REFERER = "https://www.farming-simulator.com/mods.php?lang=de&country=de&title=fs2017"

HOST = "https://www.farming-simulator.com/"
URL = "mods.php?lang=de&country=de&title=fs2017&filter=latest&page={}"
START_PAGE = 0

STORE = "mods.json"


def find_row(page):
	row_start = page.find('"row"')
	row_end = page.find('"row"', row_start + 1)
	row = page[row_start:row_end]
	return row, row_start, row_end


def find_column(row):
	column_start = row.find('"mod-item"')
	column_end = row.find('"mod-item"', column_start + 1)
	column = row[column_start:column_end]
	return column, column_start, column_end


def parse_mod(column):
	a_start = column.find("<a href")
	a_end = column.find('">', a_start)
	a = column[a_start:a_end].split('"')[1]
	label_start = column.find('"mod-label"')
	label_end = column.find("</div>", label_start)
	if label_end - label_start > 0:
		label = column[label_start:label_end].split(">")[1]
	else:
		label = ""
	name_start = column.index("<h4")
	name_end = column.index("</h4>", name_start)
	name = column[name_start:name_end].split(">")[1]
	mod_id = list(filter(lambda x: "id" in x, a.split("&")))[0].split("=")[1]
	now = str(datetime.datetime.now())
	mod = {"link": a, "label": label, "name": name, "last_crawled": now}
	return mod_id, mod


def parse_page(page):
	mods = {}
	while '"row"' in page:
		row, start, end = find_row(page)
		page = page[end:]
		while '"mod-item"' in row:
			column, c_start, c_end = find_column(row)
			row = row[c_end:]
			mod_id, mod = parse_mod(column)
			mods[mod_id] = mod
	return mods


def pagination(page):
	params = "&title=fs2017&filter=latest&page="
	page_max = START_PAGE
	index = page.find(params)
	while index >= 0:
		count = page.find('"', index)
		n = int(page[index + len(params) : count])
		page_max = max(page_max, n)
		index = page.find(params, index + 1)
	return page_max


def crawl(mods=None, url=URL):
	if mods is None:
		mods = {}
	current_page = START_PAGE
	max_page = current_page + 1
	s = requests.Session()
	s.headers.update({"User-Agent": USER_AGENT})
	while current_page <= max_page:
		logging.info("page: " + str(current_page))
		r = s.get(HOST + url.format(current_page))
		max_page = pagination(r.text)
		new_mods = parse_page(r.text)
		for mod in new_mods:
			new_mod, version = crawl_detail(s, new_mods[mod])
			if mod in mods:
				if "versions" not in mods[mod]:
					mods[mod]["versions"] = {}
				if version not in mods[mod]["versions"]:
					logging.info("new version: " + new_mod["name"] + " " + version)
					mods[mod]["versions"][version] = new_mod["versions"][version]
				mods[mod]["last_crawled"] = new_mod["last_crawled"]
				mods[mod]["label"] = new_mod["label"]
			else:
				logging.info("new mod: " + new_mod["name"])
				mods[mod] = new_mod
		current_page += 1
	return mods


def crawl_detail(session, mod):
	r = session.get(HOST + mod["link"])
	logging.debug(HOST + mod["link"])
	details = parse_detail(r.text)
	mod["versions"] = {}
	mod["versions"][details["version"]] = details
	return mod, details["version"]


def parse_detail(page):
	version = detail_helper(page, "<b>Version</b>")
	logging.debug(version)
	author = parse_author(page)
	release = detail_helper(page, "<b>Veröffentlichung</b>")
	urls = []
	matches = re.findall("(?P<url>https://[^\s]+.(zip|exe))", page)
	for match in matches:
		urls.append(match[0])
	return {"author": author, "version": version, "date": release, "urls": urls}


def parse_author(page, key="<b>Autor</b>"):
	start = page.find(key)
	end = page.find("</a>", start)
	return page[start:end].split(">")[-1]
	

def detail_helper(page, detail):
	d_start = page.find(detail)
	logging.debug(detail + " " + str(d_start))
	d_end = page.find("</div>", d_start + 10 + len(detail))
	logging.debug(d_end)
	logging.debug(page[d_start:d_end])
	return page[d_start:d_end].split(">")[-1]


def download(mods, destination, overwrite=True, filter_func=lambda x, y: True, filter_urls=lambda x: True):
	s = requests.Session()
	s.headers.update({"User-Agent": USER_AGENT, "Referer": REFERER})
	if not os.path.exists(destination):
		os.mkdir(destination)
	for id in mods:
		mod = mods[id]
		for v in mod["versions"]:
			version = mod["versions"][v]
			if filter_func(mod, version):
				for url in version["urls"]:
					if not filter_urls(url):
						continue
					modname = url.split("/")[-1]
					path = os.path.join(destination, modname.split(".")[0], v)
					if not os.path.exists(path):
						logging.info("path: " + path)
						os.makedirs(path)
					filename = os.path.join(path, modname)
					if os.path.exists(filename):
						if not overwrite:
							logging.info(filename + "(" + mod["label"] + "): skip")
							continue
					logging.info(url)
					try:
						statuscode = download_helper(s, url, filename)
						logging.info(filename + " (" + mod["label"] + "): " + str(statuscode))
					except Exception as e:
						logging.error("EEEE: " + str(e))


def download_helper(session, url, filename):
	r = session.get(url, stream=True)
	with open(filename, 'wb') as output:
		shutil.copyfileobj(r.raw, output)
	return r.status_code


if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("-v", "--verbose", help="be verbose", action="store_true")
	parser.add_argument("-u", "--update", help="don't update data file", action="store_false")
	parser.add_argument("-l", "--load", help="don't load metadata file", action="store_false")
	parser.add_argument("-d", "--download", help="don't download mods", action="store_false")
	parser.add_argument("-o", "--overwrite", help="don't overwrite mods", action="store_false")
	parser.add_argument("-f", "--filter", help="download all mods", action="store_false")
	parser.add_argument("-z", "--zip", help="don't download zip mods", action="store_false")
	parser.add_argument("-e", "--exe", help="don't download exe mods", action="store_false")
	parser.add_argument("-c", "--crawl", help="don't crawl for new/updated mods", action="store_false")
	parser.add_argument("-t", "--target", help="directory to store mods in", default="mods")
	parser.add_argument("-s", "--store", help="file to store mod metadata in", default="mods.json")
	parser.add_argument("-src", "--source", help="starting URL", default=URL)
	args = parser.parse_args()
	loglevel = logging.INFO if not args.verbose else logging.DEBUG
	logging.basicConfig(level=loglevel, format='%(message)s')
	all_mods = {}
	logging.debug("asdf")
	logging.debug(args)
	if args.load:
		if not os.path.exists(args.store):
			file =open(args.store,"w+")
			file.close()
		else:
			with open(args.store) as src:
				all_mods = json.load(src)
	if args.crawl:
		all_mods = crawl(all_mods, args.source)
		if args.update:
			with open(args.store, "w") as out:
				json.dump(all_mods, out, indent=4, sort_keys=True)
	if args.download:
		if args.filter:
			filter = lambda x, y: y["author"] == "GIANTS Software"
		else:
			filter = lambda x, y: True
		if args.zip and args.exe:
			urls = lambda x: x.endswith("exe") or x.endswith("zip")
		elif args.exe:
			urls = lambda x: x.endswith("exe")
		else:
			urls = lambda x: x.endswith("zip")
		download(
			all_mods,
			args.target,
			overwrite=args.overwrite,
			filter_func=filter,
			filter_urls=urls
		)
