* removeKolon.php
	
	replace all occurences of kolons (:) in all filenames, remove any '.flv', and save it with one trailing '.flv'
	
* removePrefix.php
	
	removes prefix seperated by an underscore (_), if there is only one part remaining
	
* rename.php
	
	This is a script that moves files in all direct subfolders to current directory, and renames it to a value in the respective subfolder, in a textfile "filename.txt".
	
* renameSubfolder.php
	
	iterate all subfolders, search for one mkv in it, rename it to '<subfolder>.mkv', and save it in current directory (parent of subfolders)
	
* distribute.py
	
	all files splittable by "-" in 2 parts wil be moved in subfolder with first part as name
