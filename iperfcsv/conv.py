#!/usr/bin/python3
# -*- coding: utf-8 -*-

from sys import argv

def relevant(line):
    return "local" in line or "/sec" in line
def getKey(line):
    key = line[3]
    try:
        key = int(key)
    except:
        return None
    return key
def hosts(line):
	line = line.replace('192.168.10.13', 'host1')
	line = line.replace('192.168.20.14', 'host2')
	line = line.replace('192.168.10.15', 'host3')
	line = line.replace('192.168.3.1', 'r0')
	line = line.replace('192.168.3.2', 'r1')
	line = line.replace('192.168.4.10', 'host4')
	line = line.replace('192.168.4.11', 'host5')
	line = line.replace('192.168.10.1', 'r0')
	line = line.replace('192.168.20.1', 'r0')
	line = line.replace('192.168.4.1', 'r1')
	line = line.replace('192.168.4.12', 'host6')
	return line

def convert(file):
	lines=[]
	with open(file) as input:
		for line in input:
		    if relevant(line):
		        lines.append(line)
	merged = []
	entries = {}
	for line in lines:
		key = getKey(line)
		if key is None:
		    continue
		if "local" in line:
		    if key in entries:
		        merged.append("".join(entries[key]))
		    entries[key]=[]
		entries[key]+=line.strip("\n")
	finished="host,port,duration,Transfer,Bandwidth"
	for entry in merged:
		start = entry.find("with ")
		line = entry[start+len("with "):]
		line = line.replace(" port ",",")
		line = line.replace("[  ",",")
		line = line.replace("]  0.0-",",")
		line = line.replace("sec  ",",")
		line = line.replace("  ",",")
		line = line.replace("Mbits/sec","")
		line = line.replace("MBytes","")
		line = hosts(line)
		tokens = line.split(",")
		newline=""
		for i,token in enumerate(tokens):
		    if not i == 2:
		        newline+=token.strip()+","
		finished+="\n"+newline.strip(",")

	with open(file+".csv","w") as output:
		output.write(finished)

if __name__ == "__main__":
	convert(argv[1])
