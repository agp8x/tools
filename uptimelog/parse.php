<?php
date_default_timezone_set("Europe/Berlin");

function parseuptimelog($logfile="/opt/uptimelog"){
/*
dataarray[]:
	-startup	(unix-time)
	-uptime		(seconds)
	-uptimehm	(hh:mm)
	-shutdown	(unix-time)
*/
	$dataarray=array();
	$file=file($logfile);
	foreach($file as $line){
		$strings=explode('-#-',$line);
		$date=strtotime($strings[0]);
		$times=explode('up',$strings[1]);
		$time=trim($times[1]);
		if(stristr($time,'min')){
			$temp=explode(' ',$time);
			$time=($temp[0]<10)? "00:0".$temp[0] : "00:".$temp[0];
		}else{
			$temp=explode(':',$time);
			$time=($temp[0]<10)? "0".$temp[0].":".$temp[1] : $time;
		}
		$temp=explode(':',$time);
		$seconds=(($temp[0]*60)+$temp[1])*60;
		$starttime=$date-$seconds;
		$dataarray[]=array('startup'=>$starttime,'uptime'=>$seconds,'uptimehm'=>$time,'shutdown'=>$date);
	}
	return $dataarray;
}
function getUptime($data){
	$uptime=0;
	foreach($data as $set){
		$uptime+=$set['uptime'];
	}
	return $uptime;
}
function getMaxUptime($data){
	$max=0;
	foreach($data as $num=>$set){
		$max=($set['uptime']>$data[$max]['uptime'])? $num: $max;
	}
	return $data[$max]['uptimehm'];
}
function fancyweb($data){
	$uptime=getUptime($data);echo "<!--$uptime--".($uptime/60)."-->";
	$max=getMaxUptime($data);
	$output="<table border style='border-collapse:collapse'><tr><th>Gesamte Uptime</th><th>Gesamte Zahl Shutdown</th><th>max uptime</th></tr>
	<tr><td>".floor($uptime/60).":".($uptime%60)."</td><td>".count($data)."</td><td>".$max."</td></tr></table><br>
	<table border style='border-collapse:collapse'><tr><th>Bootzeit</th><th>Uptime</th><th>Shutdownzeit</th><th>#</th></tr>";
	$tmp=1;
	foreach($data as $line){
		$output.="<tr><td>".date("d.m.Y H:i:s",$line['startup'])."</td><td>".$line['uptimehm']."</td><td>".date("d.m.Y H:i:s",$line['shutdown'])."</td><td>".$tmp."</td></tr>\n";
		$tmp++;
	}
	$output.="</table>";
	return $output;
}
function drawSVG($values){
//Seconds per day: 86400
//Scale-divisor from 0-86400s to 0-960px: 90
	/*
	values[]:
		-startup	(unix-time)
		-uptime		(seconds)
		-uptimehm	(hh:mm)
		-shutdown	(unix-time)
	*/
	$values2=array();
	$values3=array();
	$ordered=array();
	$value4=array();
	$svg="";
	//svg base-values
	$basey=5;
	$deltay=30;
	$basex=100;
	$labelx=7;
	
	//calculate offsets (and real times for control)
	/*
	values2[]:
		-startup	(unix-time)
		-uptime		(seconds)
		-uptimehm	(hh:mm)
		-shutdown	(unix-time)
		-startoffset	(s since 00:00:00)
		-endoffset	(s since 00:00:00)
		-realstart	(hh:mm:ss)
		-realend	(hh:mm:ss)
	*/
	foreach($values as $log){
		$startoffset=date("s",$log['startup'])+(date("i",$log['startup'])*60)+(date("H",$log['startup'])*3600);
		$endoffset=date("s",$log['shutdown'])+(date("i",$log['shutdown'])*60)+(date("H",$log['shutdown'])*3600);
		$values2[]=array("startup"=>$log["startup"],"uptime"=>$log['uptime'],"uptimehm"=>$log["uptimehm"],"shutdown"=>$log["shutdown"],"startoffset"=>$startoffset,"endoffset"=>$endoffset,"realstart"=>date("H:i:s",$log['startup']),"realend"=>date("H:i:s",$log['shutdown']));
	}
	
	
	//calculate line breaks and labels
	/*
	values3[]:
		-start[]:
			+'startoffset'	(s since 00:00:00)
		-end[]:
			+'endoffset'	(s since 00:00:00)
		-label[]:
			+'label'	(dd.mm.yyyy)
		-base[]:
			='values2[x]'
	*/
	/*
	ordered['label']:
		-start[]:
			+'startoffset'	(s since 00:00:00)
		-end[]:
			+'endoffset'	(s since 00:00:00)
	*/
	foreach($values2 as $base){
		$start=array();
		$end=array();
		$label=array();
		$lap=0;
		$uptime=$base['startoffset']+$base['uptime'];
		$start[0]=$base['startoffset'];
		$label[]=date("d.m.Y",$base['startup']);
		while($uptime>86400){
			$start[]=0;
			$end[]=86400;
			$uptime-=86400;
			$label[]=date("d.m.Y",$base['startup']+(($lap+1)*86400));
			$lap++;
		}
		$end[]=$base['endoffset'];
		//order by labels
		for($i=0;$i<=$lap;$i++){
			$ordered[$label[$i]]['start'][]=($start[$i]/90);
			$ordered[$label[$i]]['end'][]=($end[$i]/90);
		}
		$values3[]=array('start'=>$start,'end'=>$end,'label'=>$label,'base'=>$base);
	}
	
	//move labels from index to field
	/*
	value4[]:
		-start[]:
			+'startoffset'	(s since 00:00:00)
		-end[]:
			+'endoffset'	(s since 00:00:00)
		-label	(dd.mm.yyyy)
	*/
	foreach($ordered as $key=>$content){
		$value4[]=array_merge($content,array('label'=>$key));
	}
	$svgprefix=svgPrefix($ordered);
	//finaly, write the actual SVG-string
	$lap=0;
	foreach($value4 as $day){
		$lap++;
		$y=$basey+($lap*$deltay);
		$svg.='<text x="'.$labelx.'" y="'.($y+7).'" class="label">'.$day['label'].'</text>'."\n";
		foreach($day['start'] as $key=>$line){
			$svg.='	<line y1="'.$y.'" y2="'.$y.'" x1="'.($line+$basex).'" x2="'.($day['end'][$key]+$basex).'" 	class="entry" />'."\n";
		}
	}
	return($svgprefix.$svg."</svg>");
}
function svgPrefix($labels){
	$basey=15;
	$deltay=30;
	$basex=100;
	$count=sizeof($labels);
	$maxy=(($count*$deltay)+$deltay);
	$svg='<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">

<svg xmlns="http://www.w3.org/2000/svg" 
xmlns:xlink="http://www.w3.org/1999/xlink" 
xmlns:ev="http://www.w3.org/2001/xml-events" 
version="1.1" 
baseProfile="full"
width="1100px" 
height="'.$maxy.'px" >
<style type="text/css" >
	<![CDATA[
		line.entry {
		stroke: blue;
		stroke-width: 10px;
		}
		line.helpline {
			stroke: lightgrey;
			stroke-width: 1px;
			stroke-dasharray: 2,2;
		}
		line.scalemarker{
			stroke: black;
			stroke-width: 1px;
		}
		text.scaletext{
			
		}
		text.label{
			
		}
		.helpline:nth-of-type(25n+2){
			stroke: black;
		}
	]]>
</style>
<rect y="0" x="0" width="1100" height="'.$maxy.'" fill="white" /> 
<line x1="100" y1="20" x2="1060" y2="20" class="scalemarker"/>';
	for($i=0;$i<=24;$i++){
		$textx=($i>9)? 90+($i*40) : 95+($i*40);
		$linex=100+($i*40);
		$svg.='	<line x1="'.$linex.'" y1="17" x2="'.$linex.'" y2="23" class="scalemarker"/> 
	<line x1="'.$linex.'" y1="23" x2="'.$linex.'" y2="'.$maxy.'" class="helpline" /> 
	<text x="'.$textx.'" y="15" class="scaletext">'.$i.'</text>';
	}
	return $svg;
}
?>
