#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
def recDir(dir, ending):
	result = list()
	for file in os.listdir(dir):
		filepath = dir + "/" + file
		if os.path.isdir(filepath) :
			result += recDir(filepath, ending)
		else:
			if filepath.endswith(ending):
				result.append(filepath)
	return result
def help():
	return("counting lines, the recursive way.\n" + 
	"Usage:: " + sys.argv[0] + " [basedir] [ending] [stfu] \n" + 
	"\tbasedir : dir to start in, if empty, '.' is used\n" + 
	"\tending : only index filenames ending with this string, defaults to ''\n" + 
	"\tstfu : tell this damned thing to stfu...\n" + 
	"agp8x")
basedir = os.getcwd()
fileending = ""
verbose = True
if len(sys.argv) > 1 :
	if sys.argv[1] == "--help":
		print(help())
		sys.exit(0)
	basedir = sys.argv[1]
	if len(sys.argv) > 2 :
		fileending = sys.argv[2]
	if len(sys.argv) > 3 and sys.argv[3] == "stfu":
		verbose = False
if not os.path.exists(basedir):
	print("the chosen directory doesn't exist")
	sys.exit(1)
files = recDir(basedir, fileending)
linestotal = sizetotal = 0.0
print("filename \t : \t lines\t - chars\t = chars/line")
for file in files:
	size = 0.0
	file2 = open(file)
	lines = file2.readlines()
	file2.close()
	linecount = len(lines)
	for line in lines:
		size += len(line)
	if verbose:
		print(file+"\t : \t"+str(linecount)+"\t - "+str(size)+" \t = "+str(size/linecount))
	linestotal += linecount
	sizetotal += size
fileno = len(files)
if(fileno > 0):
	print(str(fileno) + " Dateien:")
	print("\t- " + str(int(linestotal)) + " Zeilen")
	print("\t- " + str(int(sizetotal)) + " Zeichen")
	print("\t- " + str(sizetotal / linestotal) + " Zeichen/Zeile")
	print("\t- " + str(linestotal / fileno) + " Zeilen/Datei")
	print("\t- " + str(sizetotal / fileno) + " Zeichen/Datei")
else:
	print("no files ending with '" + fileending + "' found... :(\n")
