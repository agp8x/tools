// ==UserScript==
// @name        flexible keyboard navigation
// @namespace   web.agp8x.org
// @description Navigate the web with your keyboard
// @author Agp8x | agp8x.org
// @version     5
// @grant       none
// @require     http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js
// @include     http://www.agrartechnik-im-einsatz.de/de/index.php?page=view_picture&*
// @include     https://xkcd.com/*
// @include     http://www.explainxkcd.com/wiki/index.php/*
// @include     http://www.heise.de/forum/*
// @include		http://www.thedailywtf.com/articles/*
// @include		http://thedailywtf.com/articles/*
// @include		http://www.commitstrip.com/*

// ==/UserScript==

this.$ = this.jQuery = jQuery.noConflict(true);

var identifiers = {
	ArrowRight: 
	   [ 'a.buttonplusl:nth-child(3)', //AiE: next
		 'ul.comicNav:nth-child(2) > li:nth-child(4) > a:nth-child(1)', //xkcd: next
		 '.no-link-underline > li:nth-child(4) > a:nth-child(1)', //explain xkcd
		 'section.metaline:nth-child(1) > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(1) > ul:nth-child(1) > li:nth-child(3) > a:nth-child(1)', //heise post
		 '.next-article > a:nth-child(1)', //daily wtf next
		 'a.nextpostslink' //commitstrip.com
		],
	ArrowLeft: 
		[ '#col3_content > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)', //AiE: prev
		 'ul.comicNav:nth-child(2) > li:nth-child(2) > a:nth-child(1)', //xkcd:prev 
		 '.no-link-underline > li:nth-child(2) > a:nth-child(1)',//explain xkcd
		 'section.metaline:nth-child(1) > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(1) > ul:nth-child(1) > li:nth-child(1) > a:nth-child(1)', //heise post
		 '.previous-article > a:nth-child(1)', //daily wtf prev
		 'a.previouspostslink'//commitstrip.com
		],
	PageDown: 
		[ 'section.metaline:nth-child(1) > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(2) > ul:nth-child(1) > li:nth-child(3) > a:nth-child(1)' //heise thread
		],
	PageUp: 
		[ 'section.metaline:nth-child(1) > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(2) > ul:nth-child(1) > li:nth-child(1) > a:nth-child(1)' //heise thread
		]
	};

$(document).keypress(function (event) {
	if (event.key in identifiers){
		for (var i in identifiers[event.key]){
			var id = identifiers[event.key][i];
			var url = $(id).attr('href');
			if (url == undefined) {
				//console.log("undefined action: "+key +" :"+url);
			} else {
				//console.log("switch to: "+ $(id).attr("href"));
				window.location = $(id).attr('href');
			}
		}
	}
});
